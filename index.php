<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once './resources/includes/head.php'?>
</head>

<body>
    <div class="container-fluid fixed-top">
        <div class="row">
        <?php include_once './resources/layout/header.php'?>

        <?php include_once './resources/layout/sidebar.php'?>

        </div>
    </div>

    <div class="container-fuild" style="margin-top: 170px">
    <?php include_once './resources/layout/slide.php'?>
    </div>

    <div class="container">
        <div id="man">
            <div class="title text-center">
                <h1>Đồng hồ nam</h1>
                <div class="title_border"></div>
            </div>
        </div>
             <?php include_once './resources/views/product_men.php'?>
        </div>
    </div>


    <div id="banner2"></div>

    <!-- ĐỒNG HỒ NỮ -->

    <div class="container">
        <div id="woman">
            <div class="title text-center">
                <h1>Đồng hồ nữ</h1>
                <div class="title_border"></div>
            </div>
        </div>
        <?php include_once './resources/views/product_women.php'?>

    </div>

    <div class="container-fuild gap"></div>



    <!-- ĐỒNG HỒ ĐÔI -->

    <div class="container">
        <div id="couple">
            <div class="title text-center">
                <h1>Đồng hồ đôi</h1>
                <div class="title_border"></div>
            </div>
        </div>

        <?php include_once './resources/views/product_couple.php'?>

    </div>

    <!-- ACCESSORY -->

    <div class="container">
        <div id="accessory">
            <div class="title text-center">
                <h1>Phụ Kiện</h1>
                <div class="title_border"></div>
            </div>
        </div>

        <?php include_once './resources/views/product_accessory.php'?>

    </div>

    <div id="more-infomation">
        <div class="title text-center">
            <h1>Thông tin hữu ích</h1>
            <div class="title_border"></div>
        </div>
    </div>


      <div class="container">
        <?php include_once './resources/layout/footer_info.php'?>

      </div>

      <div class="container-fuild gap"></div>

      <?php include_once './resources/layout/footer.php'?>

      </div>

      <?php include_once './resources/includes/script.php'?>
    </body>
</html>
<?php include_once './resources/includes/script.php'?>
